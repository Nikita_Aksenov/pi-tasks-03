#define N 256
#include <stdio.h>
int main()
{
	char string[N];
	int Sum[256] = { 0 }, i = 0;
	fgets(string, N, stdin);
	while (string[i] != '\n')
	{
		Sum[(int)string[i]]++;
		i++;
	}
	for (i = 0; i < 256; i++)
	{
		if (Sum[i] != 0)
			printf("%c - %d\n", i, Sum[i]);
	}
	return 0;
}