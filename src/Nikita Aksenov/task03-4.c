#define MaxRandomNumber 1000
#define N 10
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int max(int *arr, int size)
{
	int t = 0;
	for (int i = 0; i < size; i++)
		if (arr[i] > t)
			t = arr[i];
	return t;
}
int min(int *arr, int size)
{
	int m = MaxRandomNumber + 1;
	for (int i = 0; i < size; i++)
		if (arr[i] <  m)
			m = arr[i];
	return m;
}
float average(int *arr, int size)
{
	double sum = 0.0;
	int i = 0;
	for (i; i < size; i++)
		sum = sum + arr[i];
	return (float)sum / size;
}
int main()
{
	int arr[N];
	int i = 0;	
	srand(time(NULL));
	for (i; i < N; i++)
	{
		arr[i] = rand() % MaxRandomNumber;
		printf("%d ", arr[i]);
	}
	printf("\nMaximum = %d\n", max(arr, N));
	printf("Minimum = %d\n", min(arr, N));
	printf("Average = %.2f\n", average(arr, N));
	return 0;
}